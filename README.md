# Alar MapService

### Зависимости
* python 3.10+
* poetry

### Установка зависимостей
```shell
poetry install
```

### Запуск сервиса
##### локально в виртуальном окружении:
```shell
uvicorn app.main:app --host "0.0.0.0" --port "3000"
```
##### в docker-compose:
```shell
docker-compose -f docker-compose.yml up
```

### Миграции

###### Создание миграции
```shell
alembic revision --autogenerate -m "message"
```

###### Применение миграции
```shell
alembic upgrade head
```

###### Откат миграции
```shell
alembic downgread <hash_migrate>
```

### Переменные окружения
* `DATABASE_URL` - строка подключения к базе данных
* `TOKEN_VALIDATION` - необходимость валидации пользователя, по-умолчанию включено
* `USER_TOKEN_VALIDATE_URL` - URL-адрес валидации Bearer-токена пользователя
* `ENV` - `production` для прода, для других случаев `development`
* `RABBITMQ_URL` - строка подключения к RabbitMQ
* `PUBLISH_QUEUE` - очередь отправки сообщений RabbitMQ, по-умолчанию `route_creator`
* `CONSUME_QUEUE` - очередь приема сообщений RabbitMQ, по-умолчанию `api`
* `MAX_COORDINATES` - максимальное разрешение координат для одного запроса списка точек, по-умолчанию 10_000_000 

## API

### Точки
#### Создание точки
###### Request
```json
POST http://localhost:3001/api/v1/point
{
  "title": "string",
  "pos_x": 0,
  "pos_y": 0
}
```
###### Response
```json
{
  "title": "string",
  "pos_x": 0,
  "pos_y": 0,
  "id": 1
}
```

#### Получение списка точек
###### Request
```json
GET http://localhost:3001/api/v1/point
```
###### Response
```json
[
  {
    "title": "string",
    "pos_x": 0,
    "pos_y": 0,
    "id": 1
  }
]
```

#### Получение точки по ID
###### Request
```json
POST http://localhost:3001/api/v1/point/<point_id:int>
```
###### Response
```json
{
  "title": "string",
  "pos_x": 0,
  "pos_y": 0,
  "id": 1
}
```

### Маршруты
#### Создание маршрута
###### Request
```json
POST http://localhost:3001/api/v1/route
{
  "title": "string",
  "start_point_id": 1,
  "end_point_id": 4
}
```
###### Response
###### При создании маршрута список нод всегда null
```json
{
  "title": "11",
  "id": 34,
  "nodes": null
}
```

#### Получение списка маршрутов
###### Request
```json
GET http://localhost:3001/api/v1/route
```
###### Response
```json
[
  {
    "title": "11",
    "id": 2,
    "user": {
      "id": 1,
      "login": "User1"
    },
    "nodes": [
      {
        "route_id": 34,
        "pos": 1,
        "point_id": 1
      },
      {
        "route_id": 34,
        "pos": 2,
        "point_id": 1
      },
      {
        "route_id": 34,
        "pos": 3,
        "point_id": 4
      },
      {
        "route_id": 34,
        "pos": 4,
        "point_id": 4
      }
    ]
  },
  ...
]
```

#### Получение маршрута по ID
###### Request
```json
POST http://localhost:3001/api/v1/route/<route_id:int>
```
###### Response
```json
{
  "title": "11",
  "id": 2,
  "user": {
    "id": 1,
    "login": "User1"
  },
  "nodes": [
    {
      "route_id": 34,
      "pos": 1,
      "point_id": 1
    },
    {
      "route_id": 34,
      "pos": 2,
      "point_id": 1
    },
    {
      "route_id": 34,
      "pos": 3,
      "point_id": 4
    },
    {
      "route_id": 34,
      "pos": 4,
      "point_id": 4
    }
  ]
}
```

### Отчеты
#### Получение отчета по маршрутам
###### Request
```json
POST http://localhost:3001/api/v1/report/routes_count>
```
###### Response
```json
[
  {
    "login": "User1",
    "routes_count": 25,
    "routes_length": 126
  },
  ...
]
```

### Healthcheck
###### Request
```json
POST http://localhost:3001/api/v1/healthcheck
```
###### Response
```json
{
  "status": "alive"
}
```

### Обмен RabbitMQ сообщениями с воркерами построения маршрутов
### Формат передачи сообщений RabbitMQ
`Routing_key = <PUBLISH_QUEUE>`
```json
{
  "id": 34, 
  "title": "route_name", 
  "start_point_id": 1, 
  "end_point_id": 2
}
```
### Формат приема сообщений RabbitMQ
`Routing_key = <CONSUME_QUEUE>`
```json
{
  "id": 34, 
  "points": [1, 56, 45, 5]
}
```
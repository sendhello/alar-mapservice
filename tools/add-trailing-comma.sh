DIRS='
app/
'
for DIR in $DIRS
do
  find $DIR -type f -name '*.py' -exec add-trailing-comma --py35-plus {} \;
done

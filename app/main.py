import asyncio

from app.api import api_router
from app.constants import PROJECT_NAME, __version__
from app.db import database

from . import App

app = App(
    title=PROJECT_NAME,
    version=__version__,
)

app.include_router(api_router, prefix='/api/v1')


@app.on_event('startup')
async def startup():
    await database.connect()

    # Подключение к очереди RabbitMQ
    loop = asyncio.get_running_loop()
    task = loop.create_task(app.pika_client.consume(loop))
    await task


@app.on_event('shutdown')
async def shutdown():
    await database.disconnect()

from sqlalchemy import and_, insert, select

from app.db.models import Node, Point, Route, User


def get_points_query(left_point, right_point, top_point, bottom_point):
    return select(Point).where(
        and_(
            Point.pos_x >= left_point,
            Point.pos_x <= right_point,
            Point.pos_y >= top_point,
            Point.pos_y <= bottom_point,
        ),
    )


def get_point_query(point_id):
    return select(Point).where(Point.id == point_id)


def set_points_query():
    return insert(Point)


def get_routes_query():
    return select(Route)


def get_route_query(route_id):
    return select(Route).where(Route.id == route_id)


def set_route_query():
    return insert(Route)


def get_node_by_route_id_query(route_id):
    return select(Node).where(Node.route_id == route_id)


def set_node_query():
    return insert(Node)


def create_user_query():
    return insert(User)


def get_user_by_login_query(login: str):
    return select(User).where(User.login == login)


def get_user_query(user_id: int):
    return select(User).where(User.id == user_id)


def get_report_route_count_query():
    """Запрос на количество и протяженность маршрутов по пользователям
    """
    return """
        WITH routes_nodes AS (
            SELECT r.user_id   as user_id,
                   count(n.id) as routes_length
            FROM routes r
                     LEFT JOIN nodes n on r.id = n.route_id
            GROUP BY r.id
        )
        SELECT u.login                            as login,
        count(rn.user_id)                  as routes_count,
        coalesce(sum(rn.routes_length), 0) as routes_length
        FROM users u
                 LEFT JOIN routes_nodes rn on u.id = rn.user_id
        GROUP BY u.login;
    """

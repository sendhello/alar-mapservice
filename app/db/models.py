from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from . import Base


class Point(Base):
    __tablename__ = 'points'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, unique=True, index=True)
    pos_x = Column(Integer, index=True)
    pos_y = Column(Integer, index=True)
    nodes = relationship('Node', uselist=True, backref='point')


class Route(Base):
    __tablename__ = 'routes'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, unique=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id', ondelete='CASCADE'))
    nodes = relationship('Node', uselist=True, backref='route')


class Node(Base):
    __tablename__ = 'nodes'

    id = Column(Integer, primary_key=True, index=True)
    route_id = Column(Integer, ForeignKey('routes.id', ondelete='CASCADE'))
    pos = Column(Integer)
    point_id = Column(Integer, ForeignKey('points.id', ondelete='SET NULL'))


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    login = Column(String, unique=True, index=True)
    routes = relationship('Route', uselist=True, backref='user')

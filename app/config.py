from starlette.config import Config

config = Config('.env')

ENV: str = config('ENV', default='production')

DATABASE_URL: str = config('DATABASE_URL')

RABBITMQ_URL: str = config('RABBITMQ_URL')
PUBLISH_QUEUE: str = config('PUBLISH_QUEUE', default='route_creator')
CONSUME_QUEUE: str = config('CONSUME_QUEUE', default='api')

TOKEN_VALIDATION: bool = config('TOKEN_VALIDATION', cast=bool, default=True)
TOKEN_VALIDATE_URL: str = config('TOKEN_VALIDATE_URL', default='https://')

MAX_COORDINATES: int = config('MAX_COORDINATES', default=10_000_000)

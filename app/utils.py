from fastapi import HTTPException, status

from app.config import MAX_COORDINATES


def get_min_square_area(start_point_x: int, start_point_y: int, end_point_x: int, end_point_y: int) -> dict:
    """Получение минимальной области координат для двух точек.
    """
    left_point = min(start_point_x, end_point_x)
    right_point = max(start_point_x, end_point_x)
    top_point = min(start_point_y, end_point_y)
    bottom_point = max(start_point_y, end_point_y)

    if (right_point - left_point) * (bottom_point - top_point) > MAX_COORDINATES:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Request too large area')

    return {
        'left_point': left_point,
        'right_point': right_point,
        'top_point': top_point,
        'bottom_point': bottom_point,
    }

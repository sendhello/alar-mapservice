import json
import logging
import uuid

import pika
from aio_pika import connect_robust

from app.config import CONSUME_QUEUE, PUBLISH_QUEUE, RABBITMQ_URL

logger = logging.getLogger(__name__)


class PikaClient:

    def __init__(self, process_callable):
        self.publish_queue_name = PUBLISH_QUEUE
        url_params = pika.URLParameters(RABBITMQ_URL)
        params = pika.ConnectionParameters(
            host=url_params.host,
            port=url_params.port,
            credentials=url_params.credentials,
            heartbeat=600,
            blocked_connection_timeout=300,
        )
        self.connection = pika.BlockingConnection(params)
        self.channel = self.connection.channel()
        self.publish_queue = self.channel.queue_declare(queue=self.publish_queue_name)
        self.callback_queue = self.publish_queue.method.queue
        self.response = None
        self.process_callable = process_callable
        logger.info('Pika connection initialized')

    async def consume(self, loop):
        """Настройка канала для асинхронного приема сообщений.
        """
        connection = await connect_robust(RABBITMQ_URL, loop=loop)
        channel = await connection.channel()
        queue = await channel.declare_queue(CONSUME_QUEUE)
        await queue.consume(self.process_incoming_message, no_ack=False)
        logger.info('Established pika async listener')
        return connection

    async def process_incoming_message(self, message):
        """Обработка входящего сообщения из очереди RabbitMQ.
        """
        try:
            body = message.body
            if body:
                logger.info('Received message')
                await self.process_callable(json.loads(body))
                await message.ack()
            else:
                error_message = 'Incoming message not have body'
                logger.error(error_message)
                raise RuntimeError(error_message)

        except Exception:
            await message.reject()

    def send_message(self, message: dict):
        """Отправка сообщений в очередь RabbitMQ.
        """
        self.channel.basic_publish(
            exchange='',
            routing_key=self.publish_queue_name,
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=str(uuid.uuid4()),
            ),
            body=json.dumps(message),
        )

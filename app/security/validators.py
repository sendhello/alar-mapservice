from sqlite3 import IntegrityError

import requests
from asyncpg.exceptions import UniqueViolationError
from fastapi import Depends, HTTPException, status
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from app.config import ENV, TOKEN_VALIDATE_URL, TOKEN_VALIDATION
from app.db import database
from app.db.queries import create_user_query, get_user_by_login_query
from app.schemas.token import Token
from app.schemas.user import User, ValidateResponse

security = HTTPBearer()


async def validation_request(credentials: HTTPAuthorizationCredentials = Depends(security)) -> ValidateResponse:
    """Производит запрос в сервис пользователей, и возвращает ответ.
    """
    token = Token(access_token=credentials.credentials)
    verify_ssl = ENV == 'production'

    # При отключенной валидации пользователя, устанавливаем пользователя 'anonymous'
    if not TOKEN_VALIDATION:
        return ValidateResponse(
            valid=True,
            user=User(login='anonymous'),
        )

    res = requests.post(TOKEN_VALIDATE_URL, json=token.dict(), verify=verify_ssl)
    return ValidateResponse.parse_obj(res.json())


async def validate_response(response: ValidateResponse = Depends(validation_request)) -> User:
    """Проверяет валидность токена по ответу из сервиса пользователей.
    """
    if not response.valid:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Token is not valid')

    db_user = await database.fetch_one(
        query=get_user_by_login_query(login=response.user.login),
    )
    if not db_user:
        try:
            user_id = await database.execute(query=create_user_query(), values={'login': response.user.login})
            return User.parse_obj({**response.user.dict(), 'id': user_id})

        # Возможно пока шла проверка на существование юзера, он уже создался другим запросом,
        # и попытка его создания вызовет ошибку - тогда запрашиваем его снова
        except (IntegrityError, UniqueViolationError):
            db_user = await database.fetch_one(
                query=get_user_by_login_query(login=response.user.login),
            )

    return User.parse_obj(db_user)

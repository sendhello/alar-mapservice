from fastapi import Depends

from .validators import validate_response

PROTECTED = [Depends(validate_response)]

from sqlite3 import IntegrityError
from typing import Optional, Union

from asyncpg.exceptions import UniqueViolationError
from fastapi import APIRouter, HTTPException, status

from app.db import database
from app.db.queries import get_point_query, get_points_query, set_points_query
from app.schemas.points import Point, PointCreate
from app.utils import get_min_square_area

api_router = APIRouter()


@api_router.get('/', response_model=list[Point])
async def get_points(start_point_x: int = 0, start_point_y: int = 0, end_point_x: int = 0, end_point_y: int = 0):
    """Возвращает все существующие точки.
    """
    min_area = get_min_square_area(start_point_x, start_point_y, end_point_x, end_point_y)
    res = await database.fetch_all(
        query=get_points_query(**min_area),
    )
    if not res:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Points not found')

    return res


@api_router.get('/{point_id}', response_model=Point)
async def get_point(point_id: int):
    """Возвращает точку по ее ID.
    """
    res = await database.fetch_one(
        query=get_point_query(point_id=point_id),
    )
    if not res:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Point with id `{point_id}` not found')

    return res


@api_router.post('/', response_model=Optional[Point])
async def set_points(values: Union[PointCreate, list[PointCreate]]):
    """Добавляет новую точку/точки.

    ### Передача точек через парамер values:
    Если в values передан словарь - добавляет одну точку. При этом возвращает добавленную точку

    Если в values передан список словарей - добавляется несколько точек. Возвращается null.
    """
    try:
        if isinstance(values, PointCreate):
            point_id = await database.execute(query=set_points_query(), values=values.dict())
            return await database.fetch_one(
                query=get_point_query(point_id=point_id),
            )

        else:
            await database.execute_many(query=set_points_query(), values=[v.dict() for v in values])

    except (IntegrityError, UniqueViolationError):
        if isinstance(values, PointCreate):
            error_detail = f'Point with title `{values.title}` already exists'
        else:
            error_detail = 'Point with some title already exists'

        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=error_detail,
        )

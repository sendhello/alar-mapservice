from fastapi import APIRouter

from app.security import PROTECTED

from . import healthcheck, points, reports, routes

api_router = APIRouter(
    responses={
        404: {"description": "Page not found"},
    },
)
api_router.include_router(points.api_router, prefix='/point', dependencies=PROTECTED)
api_router.include_router(routes.api_router, prefix='/route', dependencies=PROTECTED)
api_router.include_router(reports.api_router, prefix='/report', dependencies=PROTECTED)
api_router.include_router(healthcheck.api_router, prefix='/healthcheck')

from sqlite3 import IntegrityError

from asyncpg.exceptions import UniqueViolationError
from fastapi import APIRouter, Depends, HTTPException, Request, status

from app.db import database
from app.db.queries import (
    get_node_by_route_id_query,
    get_route_query,
    get_routes_query,
    get_user_query,
    set_route_query,
)
from app.schemas.nodes import Node
from app.schemas.routes import Route, RouteBase, RouteCreate
from app.schemas.user import User
from app.security.validators import validate_response

api_router = APIRouter()


@api_router.get('/', response_model=list[Route])
async def get_routes():
    """Возвращает все существующие маршруты.
    """
    db_routes = await database.fetch_all(query=get_routes_query())
    if not db_routes:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Routes not found')

    routes = []

    for db_route in db_routes:
        route = Route.parse_obj(db_route)
        db_user = await database.fetch_one(
            query=get_user_query(user_id=db_route.user_id),
        )
        db_nodes = await database.fetch_all(
            query=get_node_by_route_id_query(route_id=route.id),
        )
        route.user = User.parse_obj(db_user)
        route.nodes = [Node.parse_obj(db_node) for db_node in db_nodes]
        routes.append(route)

    return routes


@api_router.get('/{route_id}', response_model=Route)
async def get_route(route_id: int):
    """Возвращает маршрут по его ID.
    """
    db_route = await database.fetch_one(
        query=get_route_query(route_id=route_id),
    )
    if not db_route:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Route with id `{route_id}` not found')

    db_user = await database.fetch_one(
        query=get_user_query(user_id=db_route.user_id),
    )
    db_nodes = await database.fetch_all(
        query=get_node_by_route_id_query(route_id=route_id),
    )

    route = Route.parse_obj(db_route)
    route.user = User.parse_obj(db_user)
    route.nodes = [Node.parse_obj(db_node) for db_node in db_nodes]

    return route


@api_router.post('/', response_model=Route)
async def set_route(route: RouteCreate, request: Request, current_user: User = Depends(validate_response)):
    """Добавляет новый маршрут.
    """
    try:
        route_id = await database.execute(
            query=set_route_query(),
            values={**RouteBase.parse_obj(route).dict(), 'user_id': current_user.id},
        )

    except (IntegrityError, UniqueViolationError):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f'Route with title `{route.title}` already exists',
        )

    request.app.pika_client.send_message({
        'id': route_id,
        **route.dict(),
    })

    db_route = await database.fetch_one(
        query=get_route_query(route_id=route_id),
    )
    db_user = await database.fetch_one(
        query=get_user_query(user_id=db_route.user_id),
    )

    route_res = Route.parse_obj(db_route)
    route_res.user = User.parse_obj(db_user)
    return route_res

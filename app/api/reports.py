from fastapi import APIRouter

from app.db import database
from app.db.queries import get_report_route_count_query
from app.schemas.reports import UserRouteReport

api_router = APIRouter()


@api_router.get('/routes_count', response_model=list[UserRouteReport])
async def set_route():
    """Просмотр количества маршрутов у каждого пользователя.
    """
    db_report = await database.fetch_all(
        query=get_report_route_count_query(),
    )

    return db_report

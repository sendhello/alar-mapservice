from pydantic import BaseModel, Field


class PointBase(BaseModel):
    """Точка карты.
    """
    title: str = Field(title='Наименование точки')
    pos_x: int = Field(title='Координата X')
    pos_y: int = Field(title='Координата Y')


class Point(PointBase):
    id: int = Field(title='ID точки')

    class Config:
        orm_mode = True


class PointCreate(PointBase):
    pass

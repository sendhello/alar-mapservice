from typing import Optional

from pydantic import BaseModel


class User(BaseModel):
    """Пользователь, полученный из сервиса пользователей.
    """
    id: Optional[int]
    login: str


class ValidateResponse(BaseModel):
    """Ответ валидации от сервиса пользователей.
    """
    valid: bool
    user: Optional[User]

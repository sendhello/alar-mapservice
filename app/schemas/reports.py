from pydantic import BaseModel, Field


class UserRouteReport(BaseModel):
    """Отчет по маршрутам пользователей.
    """
    login: str = Field(title='Логин пользователя')
    routes_count: int = Field(title='Количество маршрутов')
    routes_length: int = Field(title='Общая протяженность маршрутов')

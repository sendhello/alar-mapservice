from pydantic import BaseModel, Field


class NodeBase(BaseModel):
    """Маршрут.
    """
    route_id: int = Field(title='ID маршрута')
    pos: int = Field(title='Порядковый номер узла в маршруте')
    point_id: int = Field(title='ID точки карты')


class Node(NodeBase):
    id: int = Field(title='ID узла маршрута')

    class Config:
        orm_mode = True


class NodeCreate(NodeBase):
    pass

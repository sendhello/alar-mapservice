from typing import Optional

from pydantic import BaseModel, Field

from app.schemas.nodes import NodeCreate
from app.schemas.user import User


class RouteBase(BaseModel):
    """Маршрут.
    """
    title: Optional[str] = Field(title='Наименование маршрута')


class Route(RouteBase):
    id: int = Field(title='ID маршрута')
    user: Optional[User] = Field(title='Владелец маршрута')
    nodes: Optional[list[NodeCreate]] = Field(title='Путь')

    class Config:
        orm_mode = True


class RouteCreate(RouteBase):
    start_point_id: int = Field(title='ID точки начала маршрута')
    end_point_id: int = Field(title='ID точки конца маршрута')


class RouteRmqIn(BaseModel):
    id: int = Field(title='ID маршрута')
    points: Optional[list[int]] = Field(title='Список ID точек маршрута')

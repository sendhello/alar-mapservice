from fastapi import FastAPI

from app.db import database
from app.db.queries import set_node_query
from app.rmq import PikaClient
from app.schemas.nodes import NodeCreate
from app.schemas.routes import RouteRmqIn


class App(FastAPI):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pika_client = PikaClient(self.set_route)

    @classmethod
    async def set_route(cls, message: dict):
        """Запись полученного маршрута в БД.
        """
        route = RouteRmqIn.parse_obj(message)

        await database.execute_many(
            query=set_node_query(),
            values=[
                NodeCreate(route_id=route.id, point_id=point_id, pos=pos).dict()
                for pos, point_id in enumerate(route.points)
            ],
        )
